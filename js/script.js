var correctionShowHideBtn = document.getElementById("correctionShowHideBtn");

correctionShowHideBtn.addEventListener("click", correctionShowHide);

function correctionShowHide() {
    console.log("function : correctionShowHide");

    var correctionArr = document.getElementsByClassName("correction");
    
    if (correctionShowHideBtn.innerHTML == "Show") {
	for (i = 0; i < correctionArr.length; i ++) {
	    correctionArr[i].classList.add("show");
	    correctionShowHideBtn.innerHTML = "Hide";
	}
    } else {
	for (i = 0; i < correctionArr.length; i ++) {
	    correctionArr[i].classList.remove("show");
	    correctionShowHideBtn.innerHTML = "Show";
	}
    }
}
