<?php

function rft($content, $lang) {

    include "regex/" . $lang . ".php";

    foreach ($regex as $input => $output) {

	     $content = preg_replace($input, "<span class=\"correction\">" . $output . "</span>", $content); 

    }

    echo $content;

}

?>
